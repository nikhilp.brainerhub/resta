<?php
/*
Plugin Name: WooCommerce Export Orders
Plugin URI: https://imaginate-solutions.com/
Description: This plugin lets store owners to export orders
Version: 1.1.0
Author: Dhruvin Shah
Author URI: https://imaginate-solutions.com/
Requires PHP: 5.6
WC requires at least: 3.0.0
WC tested up to: 3.5.1
*/ {
	/*** Localisation **/
	load_plugin_textdomain('woo-export-order', false, dirname(plugin_basename(__FILE__)) . '/');

	/*** woo_export class **/
	if (!class_exists('woo_export')) {

		class woo_export
		{
			public function __construct()
			{

				$this->order_status = array(
					'completed'		=> __('Completed', 'woo-export-order'),
					'cancelled'		=> __('Cancelled', 'woo-export-order'),
					'failed'		=> __('Failed', 'woo-export-order'),
					'refunded'		=> __('Refunded', 'woo-export-order'),
					'processing'	=> __('Processing', 'woo-export-order'),
					'pending'		=> __('Pending', 'woo-export-order'),
					'on-hold'		=> __('On Hold', 'woo-export-order'),
				);
				// WordPress Administration Menu
				add_action('admin_menu', array(&$this, 'woo_export_orders_menu'));

				add_action('admin_enqueue_scripts', array(&$this, 'export_enqueue_scripts_css'));
				add_action('admin_enqueue_scripts', array(&$this, 'export_enqueue_scripts_js'));
			}

			/*** Functions ***/
			function export_enqueue_scripts_css()
			{

				if (isset($_GET['page']) && $_GET['page'] == 'export_orders_page') {
					wp_enqueue_style('semantic', plugins_url('/css/semantic.min.css', __FILE__), '', '', false);

					wp_enqueue_style('semanticDataTable', plugins_url('/css/dataTables.semanticui.min.css', __FILE__), '', '', false);

					wp_enqueue_style('semanticButtons', plugins_url('/css/buttons.semanticui.min.css', __FILE__), '', '', false);

					wp_enqueue_style('dataTable', plugins_url('/css/data.table.css', __FILE__), '', '', false);
				}
			}

			function export_enqueue_scripts_js()
			{

				if (isset($_GET['page']) && $_GET['page'] == 'export_orders_page') {

					wp_register_script('woo_jquery', plugins_url() . '/woocommerce-export-orders/js/jquery.min.js');
					wp_enqueue_script('woo_jquery');

					wp_register_script('dataTable', plugins_url() . '/woocommerce-export-orders/js/jquery.dataTables.js');
					wp_enqueue_script('dataTable');

					wp_register_script('dataTableSemantic', plugins_url() . '/woocommerce-export-orders/js/dataTables.semanticui.min.js');
					wp_enqueue_script('dataTableSemantic');

					wp_register_script('dataTableButtons', plugins_url() . '/woocommerce-export-orders/js/dataTables.buttons.min.js');
					wp_enqueue_script('dataTableButtons');

					wp_register_script('buttonsSemantic', plugins_url() . '/woocommerce-export-orders/js/buttons.semanticui.min.js');
					wp_enqueue_script('buttonsSemantic');

					wp_register_script('woo_pdfmake', plugins_url() . '/woocommerce-export-orders/js/pdfmake.min.js');
					wp_enqueue_script('woo_pdfmake');

					wp_register_script('jszip', plugins_url() . '/woocommerce-export-orders/js/jszip.min.js');
					wp_enqueue_script('jszip');

					wp_register_script('vfsfonts', plugins_url() . '/woocommerce-export-orders/js/vfs_fonts.js');
					wp_enqueue_script('vfsfonts');

					wp_register_script('buttonsHTML5', plugins_url() . '/woocommerce-export-orders/js/buttons.html5.min.js');
					wp_enqueue_script('buttonsHTML5');
				}
			}
			function woo_export_orders_menu()
			{
				add_menu_page(
					'Export Orders',
					'Export Orders',
					'manage_woocommerce',
					'export_orders_page',
					'',
					'dashicons-media-spreadsheet',
					'55.7'
				);
				add_submenu_page(
					'export_orders_page.php',
					__('Export Orders Settings', 'woo-export-order'),
					__('Export Orders Settings', 'woo-export-order'),
					'manage_woocommerce',
					'export_orders_page',
					array(&$this, 'export_orders_page')
				);
			}
			function export_orders_page()
			{
				global $wpdb; ?>
				<h2 class="nav-tab-wrapper woo-nav-tab-wrapper">
					<a href="javascript:void(0);" class="nav-tab nav-tab-active">
						<?php _e('Orders', 'woo-export-order'); ?> </a>
					<a href="javascript:void(0);" class="nav-tab ">
						<button id="printorder">Print Order</button></a>
				</h2>
				<script type="text/javascript">
					function printDiv(divName) {
						var printContents = document.getElementById(divName).innerHTML;
						var originalContents = document.body.innerHTML;
						document.body.innerHTML = printContents;
						setTimeout(function() {
							window.print();
						}, 100);
						document.body.innerHTML = originalContents;
					}
				</script>

				<script type="text/javascript">
					$(document).ready(function() {
						$(document).on("click", "#printorder", function() {
							if ($(".po_status:checked").length === 0) {
								alert('please select at least one value');
								return;
							}
							$('input:checkbox:not(:checked)').each(function() {
								if (!$(this).is(':checked')) {
									let test = $(this).attr("id");
									var suffix = test.match(/\d+/); // 123456
									$(`#order${suffix}`).hide();
								} else {
									$(this).hide();
								}
							});
							$(".po_status:checked").hide();
							$("#adminmenumain, #wpadminbar, #wpfooter, .nav-tab-wrapper.woo-nav-tab-wrapper").hide();
							$("#wpcontent").css("margin-left", '0px');
							printDiv('printable');
							setTimeout(function() {
								$(".po_status").prop("checked", false).show();
								$(".po_status").parents(".order-item-hk").show();
								$("#adminmenumain, #wpadminbar, #wpfooter, .nav-tab-wrapper.woo-nav-tab-wrapper").show();
								$("#wpcontent").removeAttr("style");
							}, 300);
						});
					});
				</script>
				<div class="order-list-hk" id="printable">
					<style>
						body {
							font-family: system-ui;
							font-size: 10pt;
							padding: 0;
							margin: 0;
						}

						@page {
							margin: 0px;
							padding: 0px;
						}

						p {
							margin: 0pt;
						}

						.details td {
							border: 1px #eee solid;
							padding: 10px 8px;
						}

						table.items {
							border: 0.1mm solid #e7e7e7;
						}

						td {
							vertical-align: top;
						}

						.items td {
							border-left: 0.1mm solid #e7e7e7;
							border-right: 0.1mm solid #e7e7e7;
							text-align: left;
							width: 25%;
						}

						table thead td {
							text-align: center;
							border: 0.1mm solid #e7e7e7;
						}

						.order-item-hk {
							padding: 0px 15px 0px 0px;
							border-bottom: 1px solid #e5e5e5;
						}

						.order-item-hk th,
						.order-item-hk td {
							padding: 10px;
						}

						.company_desc {
							padding: 15px
						}

						@media print {
							.order-item-hk {
								clear: both;
								page-break-after: always;
							}
						}

						@media print {
							body {
								margin-top: -20px !important;
							}

							.pdf-left-title {
								margin-left: -25px;

							}
						}
					</style>
					<?php
					$args = array(
						'limit' => -1,
					);
					$orders = wc_get_orders($args);
					// print_r($orders[0]->data['billing']['first_name']);
					//print_r($orders);

					
					$var = $today_checkin_var = $today_checkout_var = $booking_time = "";

					foreach ($orders as $id_key => $order) {

						$order_items = $order->get_items();
						$my_order_meta = get_post_custom($order->get_id());

						$attribute_taxonomies = wc_get_attribute_taxonomies();
						$attribut_list = [];
						foreach($attribute_taxonomies as $attr){
							$attribut_list[] = $attr->attribute_label;
						}

						$c = 0;
						$dt = new DateTime($order->get_date_created());

						$date = $dt->format('d-m-Y');
						$var .= '<div class="order-item-hk" id="order' . $order->get_id() . '">
						<table class="pdf-left-title" width="100%" cellpadding="0" id="order' . $order->get_id() . '">
							<tr>
								<td><input type="checkbox" class="po_status" name="status" id="check' . $order->get_id() .
							'" value="" /></td>
								<td width="80%"><b>Codice:</b> ' . $order->get_id() . '</td>
								<td width="20%"><b>Data:</b> ' . $date . ' </td>
							</tr>
						</table>
						<br>
						<table class="items" width="100%"  style="font-size: 14px; border-collapse: collapse;" cellpadding="8">
							<thead>
								<tr>
									<th>Titolo</th>
									';
									foreach($attribut_list as $attr){
										$var .= '<th>'.$attr.'</th>';
									}
									$var .= '
									<th>QUANTITA’</th>
								</tr>';
							foreach ($order_items as $items_key => $items_value) {
								$pid = $items_value['product_id'];
								$producthh = wc_get_product($pid);

								$attributes = $producthh->get_attributes();
								$attrs = [];
								foreach($attributes as $k => $attr){
									foreach($attr['options'] as $a){
										$attrs[$k][] = get_term($a)->name;
									}
								}

								$var .= '
									<tr>
										<td>' . $items_value['name'] . '</td>';
										foreach($attrs as $k => $a){
											$var .= '<td>' . implode(", ", $a) . '</td>';
										}
										if (count($attribut_list) - count($attrs)) {
											$diff = count($attribut_list) - count($attrs);
											for($i=0; $i<$diff; $i++){
												$var .= '<td> - </td>';
											}
										}
										$var .= '<td>' . $items_value->get_quantity() . '</td>
									</tr>';
								$c++;
						}
						$var .= '</thead>
							</table>
							<table width="100%">
							<tr>
								<td>
									<table width="60%" text-align="left">
										<tr>
											<td width="80%"><strong>Dimensioni collo</strong></td>
										</tr>
									</table>
									<table width="100%" text-align="right" class="details">
										<tr>
											<td>L (cm).</td>
											<td>H (cm).</td>
											<td>W (cm).</td>
											<td>Peso(kg)</td>
											<td>Scatolo Piccolo</td>
											<td>Scatolo Piccolo</td>
										</tr>
											<tr>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
											</tr>';
										// foreach ($order_items as $items_key => $items_value) {
										// 	$pid    = $items_value['product_id'];
										// 	$producthh = wc_get_product($pid);
										// 	$patt   = $producthh->get_attributes('color');
										// 	$misure   = $producthh->get_attributes('misure-finite');
										// 	$color = $patt['color']['options'];
										// 	$misure_val = $misure['misure-finite']['options'];
										// 	$color = (!empty($color)) ? implode(" , ", $color) : '';
										// 	$misure_val = (!empty($misure_val)) ? implode(" , ", $misure_val) : '';
										// 	$var .= '
										// 	<tr>
										// 		<td>' . $producthh->get_length() . '</td>
										// 		<td>' . $producthh->get_height() . '</td>
										// 		<td>' . $producthh->get_width() . '</td>
										// 		<td>' . $producthh->get_weight() . '</td>
										// 		<td></td>
										// 		<td></td>
										// 	</tr>';
										// }
										$var .= '</table>
										</td>
										</tr>
									</table><br><br>
									<table text-align="left" width="100%" style="height:450px">
										<tr>
											<td width="100%" style="padding: 0px; text-align: left;"></td>
										</tr>
									</table>
									<table text-align="left" width="100%">
										<tr>
											<td>
												<table text-align="left" style="text-align: left; width:100%">
													<tr>
														<td style="padding: 0px; line-height: 20px; width:40%; margin-bottom: 20px;">
															<a href="#" target="_blank">
																<img src="' . plugin_dir_url(__FILE__) . '/img/RESTA-LOGO.png" width="190" height="auto" alt="Logo" text-align="center">
															</a>
															<br>
															<strong>Resta Piccolo</strong>
															<br>
															Via Circonvallazione, 5 – 73040 Aradeo (LE)
															<br>
															Tel. 0836 554258 – Fax 0836 553865 - P.IVA 03028940751
															<br>
															<a href="www.restatendaggi.it">www.restatendaggi.it</a> - info@restatendaggi.
														</td>
														<td style="width:25%">
															<table text-align="left" style="text-align: left; width: 100%">
																<tr>
																	<td style="padding: 0px; line-height: 20px;">
																		<strong>INTES</strong><br>
																		'. $order->data['billing']['first_name'].' <br>
																		'. $order->data['billing']['last_name']. '<br>
																		' . $order->data['billing']['company'] . '<br>
																		' . $order->data['billing']['address_1'] . '<br>
																		' . $order->data['billing']['address_2'] . '<br>
																		' . $order->data['billing']['city'] . '<br>
																		' . $order->data['billing']['state'] . '<br>
																		' . $order->data['billing']['postcode'] . '<br>
																		' . $order->data['billing']['country'] . '<br>
																	</td>
																</tr>
															</table>
														</td>
														<td style="padding: 0px; line-height: 20px;">
															<strong>DESTI</strong><br>
															' . $order->data['shipping']['first_name'] . ' <br>
															' . $order->data['shipping']['last_name'] . '<br>
															' . $order->data['shipping']['company'] . '<br>
															' . $order->data['shipping']['address_1'] . '<br>
															' . $order->data['shipping']['address_2'] . '<br>
															' . $order->data['shipping']['city'] . '<br>
															' . $order->data['shipping']['state'] . '<br>
															' . $order->data['shipping']['postcode'] . '<br>
															' . $order->data['shipping']['country'] . '<br>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</div>';
							}
						echo $var;
					?>
				</div>
			<?php 	}
		}
	}
	$woo_export = new woo_export();
}
?>